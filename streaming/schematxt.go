package streaming

var schemaTxt = `schema {
    query: Query
}


# The query type, represents all of the entry points into our object graph
type Query {
    sessions(): [Session]!
}
enum TranscodingState {
    NEW
    RUNNING
    # Process is paused because there is enough transcoding buffer created.
    THROTTLED 
    STOPPING
    # Done with it's job, when transmuxing this will happen almost immediately.
    EXITED 
}

type Session {
    # Filelocater of the media file currently playing
    fileLocator: String!
    # Unique ID of the transcoder session, this is shared between multiple streams (a audio and video stream that pair together will have the same ID)
    sessionID: String!
    # UserID of the stream owner
    userID: Int!
    # Percentage of transcoded content available in buffer
    transcodingPercentage: Int!
    # Whether the stream is throttled since we have enough buffer available
    throttled: Boolean!
    # Whether this is a transcoded stream
    transcoded: Boolean!
    # Whether this is a transmuxed stream
    transmuxed: Boolean!
    # Last time this stream had a segment requested by a client
    lastAccessed: String!
    # Target container for this stream
    container: String!
    # Target resolution for this stream
    resolution: String!
    # Target coded for this stream
    codecs: String!
    # Target coded name for this stream
    codecName: String!
    # Stream type (Video/Audio/Subtitle)
    streamType: String!
    # Language information for audio/subtitle stream
    language: String!
    # Title for audio/subtitle stream
    title: String!
    # Target bitrate
    bitRate: Int!
    # Current state the transcoder is in
    transcodingState: TranscodingState!
}`
